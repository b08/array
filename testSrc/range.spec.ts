import { test } from "@b08/test-runner";
import { range } from "../src";

test("range should return array of size, filled with numbers", async t => {
  // arrange
  const expected = [0, 1, 2];

  // act
  const result = range(3);

  // assert
  t.deepEqual(result, expected);
});

test("range should return array from start to stop", async t => {
  // arrange
  const expected = [3, 4, 5, 6, 7, 8];

  // act
  const result = range(3, 9);

  // assert
  t.deepEqual(result, expected);
});

test("range should revert start and stop", async t => {
  // arrange
  const expected = [3, 4, 5, 6, 7, 8];

  // act
  const result = range(9, 3);

  // assert
  t.deepEqual(result, expected);
});
