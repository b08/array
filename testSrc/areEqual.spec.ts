import { test } from "@b08/test-runner";
import { areEqual } from "../src";

test("areEqual should return true for equal arrays", async t => {
  // arrange
  const input = [1, 2, 3];
  const input2 = [1, 2, 3];

  // act
  const result = areEqual(input, input2);

  // assert
  t.true(result);
});

test("areEqual should return true for arrays with equal items", async t => {
  // arrange
  const input = [{ item: "1" }, { item: "2" }];
  const input2 = [{ item: "1" }, { item: "2" }];

  // act
  const result = areEqual(input, input2, item => item.item);

  // assert
  t.true(result);
});

test("areEqual should return false for different length", async t => {
  // arrange
  const input = [1, 2, 3];
  const input2 = [1, 2];

  // act
  const result = areEqual(input, input2);

  // assert
  t.false(result);
});

test("areEqual should return false for different content", async t => {
  // arrange
  const input = [1, 2, 3];
  const input2 = [1, 3, 2];

  // act
  const result = areEqual(input, input2);

  // assert
  t.false(result);
});
