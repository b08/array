import { test } from "@b08/test-runner";
import { flatMap } from "../src";

test("flatMap should return flattened array", async t => {
  // arrange
  const input = [[1, 2], [2, 3]];

  // act
  const result = flatMap(input);

  // assert
  t.deepEqual(result, [1, 2, 2, 3]);
});

test("flatMap should return concatenated arrays", async t => {
  // arrange
  const input = [{ items: [1, 2] }, { items: [2, 3] }];

  // act
  const result = flatMap(input, i => i.items);

  // assert
  t.deepEqual(result, [1, 2, 2, 3]);
});

test("flatMap should not add items that are not array", async t => {
  // arrange
  const map = new Map<number, number[]>([[1, [2, 3]]]);

  const input = [1, 2];

  // act
  const result = flatMap(input, i => map.get(i));

  // assert
  t.deepEqual(result, [2, 3]);
});
