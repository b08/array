import { test } from "@b08/test-runner";
import { max, min } from "../src";

test("max should return item with maximum value", async t => {
  // arrange
  const input = [{ level: 1 }, { level: 2 }, { level: 0 }];

  // act
  const result = max(input, i => i.level);

  // assert
  t.equal(result, input[1]);
});

test("max should return maximum", async t => {
  // arrange
  const input = [1, 3, 2];

  // act
  const result = max(input);

  // assert
  t.equal(result, 3);
});

test("min should return item with minimum value", async t => {
  // arrange
  const input = [{ level: 1 }, { level: 2 }, { level: 0 }];

  // act
  const result = min(input, i => i.level);

  // assert
  t.equal(result, input[2]);
});

test("min should return minimum", async t => {
  // arrange
  const input = [1, 3, 2];

  // act
  const result = min(input);

  // assert
  t.equal(result, 1);
});
