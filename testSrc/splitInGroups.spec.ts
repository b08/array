import { splitInGroups, splitInGroupsBy } from "../src";
import { test } from "@b08/test-runner";

test("splitInGroupsBy should return items unique by single field", async t => {
  // arrange
  const input = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
  const expected = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13]];

  // act
  const result = splitInGroupsBy(input, 4);

  // assert
  t.deepEqual(result, expected);
});

test("splitInGroupsBy should split into one group if count is low", async t => {
  // arrange
  const input = [1];
  const expected = [[1]];

  // act
  const result = splitInGroupsBy(input, 2);

  // assert
  t.deepEqual(result, expected);
});

test("splitInGroups should split by groups count", async t => {
  // arrange
  const input = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
  const expected = [[1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [11, 12, 13]];

  // act
  const result = splitInGroups(input, 3);

  // assert
  t.deepEqual(result, expected);
});
