import { test } from "@b08/test-runner";
import { unique } from "../src";

test("unique should return items unique by single field", async t => {
  // arrange
  const input = [{ id: 1 }, { id: 2 }, { id: 1 }];
  const expected = [{ id: 1 }, { id: 2 }];

  // act
  const result = unique(input, item => item.id);

  // assert
  t.deepEqual(result, expected);
});

test("unique should return unique values", async t => {
  // arrange
  const input = [{ id: 1 }, { id: 2 }, { id: 1 }];
  const expected = [{ id: 1 }, { id: 2 }];

  // act
  const result = unique(input, item => item.id);

  // assert
  t.deepEqual(result, expected);
});
