import { regexToArray } from "../src";
import { test } from "@b08/test-runner";

test("regexToArray should return array of found matches", async t => {
  // arrange
  const content = "aaaaaabaaaabbaaa";
  const regex = /b+/g;

  // act
  const result = regexToArray(regex, content);

  // assert
  t.equal(result.length, 2);
  t.equal(result[0][0], "b");
  t.equal(result[1][0], "bb");
});

test("regexToArray should return empty array", async t => {
  // arrange
  const content = "aaaaaabaaaabbaaa";
  const regex = /z+/g;

  // act
  const result = regexToArray(regex, content);

  // assert
  t.equal(result.length, 0);
});
