import { test } from "@b08/test-runner";
import { findIndex } from "../src";

test("findIndex should return index of fist item satisfying the predicate", async t => {
  // arrange
  const input = [1, 2, 3, 4];
  // act
  const result = findIndex(input, item => item > 2);

  // assert
  t.equal(result, 2);
});

test("findIndex should return -1 if nothing satisfies", async t => {
  // arrange
  const input = [1, 2, 1, 2];
  // act
  const result = findIndex(input, item => item > 2);

  // assert
  t.equal(result, -1);
});
