import { test } from "@b08/test-runner";
import { trim, trimOne } from "../src";

test("trim should trim 2 out of 3", async t => {
  // arrange
  const input = [1, 2, 3];
  const expected = [1];

  // act
  const result = trim(input, 2);

  // assert
  t.deepEqual(result, expected);
});

test("trim should trim 3 out of 2 to result in empty array", async t => {
  // arrange
  const input = [1, 2];
  const expected = [];

  // act
  const result = trim(input, 3);

  // assert
  t.deepEqual(result, expected);
});


test("trim should trim one", async t => {
  // arrange
  const input = [1, 2];
  const expected = [1];

  // act
  const result = trimOne(input);

  // assert
  t.deepEqual(result, expected);
});
