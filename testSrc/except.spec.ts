import { test } from "@b08/test-runner";
import { except, intersect } from "../src";

test("except should return values from first array not present in second array", async t => {
  // arrange
  const input = [1, 2, 3, 4];
  const input2 = [2, 3, 5];
  const expected = [1, 4];
  // act
  const result = except(input, input2);

  // assert
  t.deepEqual(result, expected);
});

test("intersect should return values present in both arrays", async t => {
  // arrange
  const input = [1, 2, 3, 4];
  const input2 = [2, 3, 5];
  const expected = [2, 3];
  // act
  const result = intersect(input, input2);

  // assert
  t.deepEqual(result, expected);
});
