import { contains } from "../src";
import { test } from "@b08/test-runner";

test("contains should return true if item is in array", async t => {
  // arrange
  const input = [1, 2, 3];

  // act
  const result = contains(input, 1);

  // assert
  t.true(result);
});

test("contains should return false if item is not in array", async t => {
  // arrange
  const input = [1, 2, 3];

  // act
  const result = contains(input, 4);

  // assert
  t.false(result);
});
