import { test } from "@b08/test-runner";
import { all } from "../src";

test("all should return true if all match", t => {
  // arrange
  const input = [1, 2, 3];

  // act
  const result = all(input, i => i > 0);

  // assert
  t.true(result);
});

test("all should return true for empty", t => {
  // arrange
  const input = [];

  // act
  const result = all(input, i => i > 0);

  // assert
  t.true(result);
});

test("all should return false if some don't match", t => {
  // arrange
  const input = [1, 2, 3];

  // act
  const result = all(input, i => i < 3);

  // assert
  t.false(result);
});
