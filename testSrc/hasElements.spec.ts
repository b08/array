import { test } from "@b08/test-runner";
import { isEmpty } from "../src";

test("isEmpty should return true for empty array", async t => {
  // arrange

  // act
  const result = isEmpty([]);

  // assert
  t.true(result);
});

test("isEmpty should return true for null", async t => {
  // arrange

  // act
  const result = isEmpty(null);

  // assert
  t.true(result);
});

test("isEmpty should return false for non empty array", async t => {
  // arrange

  // act
  const result = isEmpty([1]);

  // assert
  t.false(result);
});
