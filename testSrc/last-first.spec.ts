import { test } from "@b08/test-runner";
import { last, first } from "../src";

test("last should return last element", async t => {
  // arrange
  const input = [1, 2, 3, 4];

  // act
  const result = last(input);

  // assert
  t.equal(result, 4);
});

test("last should return last by Selector", async t => {
  // arrange
  const input = [1, 2, 3, 4];

  // act
  const result = last(input, i => i < 3);

  // assert
  t.equal(result, 2);
});

test("first should return first element", async t => {
  // arrange
  const input = [1, 2, 3, 4];

  // act
  const result = first(input);

  // assert
  t.equal(result, 1);
});

test("first should return first by Selector", async t => {
  // arrange
  const input = [1, 2, 3, 4];

  // act
  const result = first(input, i => i > 1);

  // assert
  t.equal(result, 2);
});
