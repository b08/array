export function range(stop: number): number[];
export function range(start: number, stop: number): number[];
export function range(start: number, stop?: number): number[] {
  if (stop == null) {
    stop = start;
    start = 0;
  }

  if (start > stop) {
    const ex = start;
    start = stop;
    stop = ex;
  }

  const result = [];
  for (let i = Math.floor(start); i < stop; i++) {
    result.push(i);
  }
  return result;
}
