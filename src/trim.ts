export function trim<T>(arr: T[], amount: number = 1): T[] {
  if (!arr) { return arr; }
  return arr.slice(0, Math.max(0, arr.length - amount));
}

export function trimOne<T>(arr: T[]): T[] {
  return trim(arr, 1);
}
