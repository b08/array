import { isEmpty } from "./hasElements";

export function max<T>(src: T[]): T;
export function max<TItem, TVal>(src: TItem[], func: (item: TItem) => TVal): TItem;
export function max<TItem, TVal>(src: TItem[], func?: (item: TItem) => TVal): TItem {
  if (!Array.isArray(src)) { return undefined; }
  if (!func) {
    return Math.max.apply(null, src);
  } else {
    return maxBy(src, func);
  }
}

export function min<T>(src: T[]): T;
export function min<TItem, TVal>(src: TItem[], func: (item: TItem) => TVal): TItem;
export function min<TItem, TVal>(src: TItem[], func?: (item: TItem) => TVal): TItem {
  if (!Array.isArray(src)) { return undefined; }
  if (!func) {
    return Math.min.apply(null, src);
  } else {
    return minBy(src, func);
  }
}


function maxBy<TItem, TVal>(src: TItem[], func: (item: TItem) => TVal): TItem {
  if (isEmpty(src)) { return undefined; }
  let i = src.length - 1;
  let maxItem = src[i];
  let maxVal = func(maxItem);
  while (i--) {
    const nextItem = src[i];
    const nextVal = func(nextItem);
    if (maxVal < nextVal) {
      maxItem = nextItem;
      maxVal = nextVal;
    }
  }
  return maxItem;
}

function minBy<TItem, TVal>(src: TItem[], func: (item: TItem) => TVal): TItem {
  if (isEmpty(src)) { return undefined; }
  let i = src.length - 1;
  let minItem = src[i];
  let minVal = func(minItem);
  while (i--) {
    const nextItem = src[i];
    const nextVal = func(nextItem);
    if (minVal > nextVal) {
      minItem = nextItem;
      minVal = nextVal;
    }
  }

  return minItem;
}
