
export function flatMap<TIn>(src: TIn[][]): TIn[];
export function flatMap<TIn, TOut>(src: TIn[], selector: (item: TIn, index: number) => TOut[]): TOut[];
export function flatMap<TIn, TOut>(src: TIn[], selector?: (item: TIn, index: number) => TOut[]): TOut[] {
  if (!Array.isArray(src)) { return undefined; }
  if (!selector) { return flatten(<any>src); }
  return src.reduce((res, item, index) => {
    const itemResult = selector(item, index);
    if (Array.isArray(itemResult) && itemResult.length) {
      res.push(...itemResult);
    }
    return res;
  }, []);
}

function flatten<TIn>(src: TIn[][]): TIn[] {
  return src.reduce((res, item) => {
    if (Array.isArray(item) && item.length) {
      res.push(...item);
    }
    return res;
  }, []);
}
