export function isEmpty(src: any[]): boolean {
  return !Array.isArray(src) || src.length === 0;
}

export function hasElements(src: any[]): boolean {
  return Array.isArray(src) && src.length > 0;
}
