export function all<T>(src: T[], func: (item: T) => boolean): boolean {
  return Array.isArray(src) && !src.some(item => !func(item));
}
