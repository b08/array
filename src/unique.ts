
export type BySelector<T> = (t: T) => any;

export function unique<T>(arr: T[], by?: BySelector<T>): T[] {
  if (!Array.isArray(arr)) { return null; }
  if (by == null) {
    return uniqueItem(arr);
  } else {
    return uniqueBySelector(arr, by);
  }
}

function uniqueItem<T>(arr: T[]): T[] {
  const set = new Set<T>(arr);
  return Array.from(set);
}

function uniqueBySelector<T>(arr: T[], by: BySelector<T>): T[] {
  const set = new Set();
  return arr.filter(item => {
    const key = by(item);
    if (set.has(key)) { return false; }
    set.add(key);
    return true;
  });
}
