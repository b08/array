import { areEqualInternal } from "./areEqualInternal";

export function areEquivalent<T>(arr1: T[], arr2: T[]): boolean;
export function areEquivalent<TItem, TVal>(arr1: TItem[], arr2: TItem[], selector: (item: TItem) => TVal): boolean;
export function areEquivalent<TItem, TVal>(arr1: TItem[], arr2: TItem[], selector?: (item: TItem) => TVal): boolean {
  if (arr1 === arr2) {
    return true;
  }

  if (!Array.isArray(arr1) || !Array.isArray(arr2) || arr1.length !== arr2.length) {
    return false;
  }

  return areEqualInternal(sort(arr1, selector), sort(arr2, selector), selector);
}

function sort<TItem, TVal>(array: TItem[], func?: (item: TItem) => TVal): TItem[] {
  if (!func) {
    return [...array].sort();
  }

  return [...array].sort((item1, item2) => {
    const val1 = func(item1);
    const val2 = func(item2);
    if (val1 < val2) {
      return -1;
    } else if (val1 > val2) {
      return 1;
    } else {
      return 0;
    }
  });
}
