export function except<T>(src: T[], exclusion: T[]): T[] {
  if (!Array.isArray(src)) { return undefined; }
  const set = new Set(exclusion);
  return src && src.filter(item => !set.has(item));
}

export function intersect<T>(src: T[], inter: T[]): T[] {
  if (!Array.isArray(src)) { return undefined; }
  const set = new Set(inter);
  return src && src.filter(item => set.has(item));
}
