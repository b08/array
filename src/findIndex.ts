export function findIndex<T>(src: T[], predicate: (item: T) => boolean): number {
  if (!Array.isArray(src)) { return -1; }

  for (let i = 0; i < src.length; i++) {
    if (predicate(src[i])) {
      return i;
    }
  }

  return -1;
}
