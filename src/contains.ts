export function contains<T>(src: T[], item: T): boolean {
  return Array.isArray(src) && src.indexOf(item) !== -1;
}
