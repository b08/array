
export function last<T>(src: T[]): T;
export function last<T>(src: T[], func: (item: T) => boolean): T;
export function last<T>(src: T[], func?: (item: T) => boolean): T {
  if (!Array.isArray(src)) { return undefined; }
  if (!func) { return src[src.length - 1]; }

  let i = src.length;
  while (i--) {
    if (func(src[i])) {
      return src[i];
    }
  }
  return undefined;
}


export function first<T>(src: T[]): T;
export function first<T>(src: T[], func: (item: T) => boolean): T;
export function first<T>(src: T[], func?: (item: T) => boolean): T {
  if (!Array.isArray(src)) { return undefined; }
  if (!func) { return src[0]; }

  for (let item of src) {
    if (func(item)) {
      return item;
    }
  }

  return undefined;
}
